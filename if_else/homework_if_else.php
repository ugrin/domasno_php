<?php
	//Prva zadaca
	$name= 'Kathrin';
	if ($name=='Kathrin')
	{
		echo "Hello Kathrin";
	}
	else
	{
		echo "Nice name";
	}
	echo '<br>';

	//Vtora zadaca
	$time = date('H');
	if($time < 12)
	{
		echo "Good Morning Kathrin";
	}
	else
	{
		echo "Good afternoon Kathrin";
	}
	echo '<br>';

	//Treta zadaca
	$rating = 12;
	if ($rating >= 1 && $rating <= 10)
	{
		echo "Thank you for rating.";
	}
	else
	{
		echo "Invalid rating.";
	}
	echo '<br>';

	//Cetvrta zadaca
	$rated = true;
	$rating = 5;
	if ($rating >=1 && $rating <=10 && $rated=true)
	{
		echo 'You already voted.';
	}
	elseif ($rating >=1 && $rating <=10 && $rated=false) 
	{
		echo 'Thank you for voting';
	}
	else
	{
		echo "Invalid rating.";
	}
	echo '<br>';

	//Petta zadaca
	$tekst = "Blaala balallala";
	if (strlen($tekst)>20 && str_word_count($tekst) > 4)
	{
		echo strtoupper($tekst);
	}
	else
	{
		echo strtolower($tekst);
	}
	echo "<br>";

	//Shesta zadaca
	$x=12;
	switch ($x) {
		case 1:
			echo "Januari";
			break;
		case 2:
			echo "Fevruari";
			break;	
		case 3:
			echo "Mart";
			break;
		case 4:
			echo "April";
			break;
		case 5:
			echo "Maj";
			break;
		case 6:
			echo "Juni";
			break;
		case 7:
			echo "Juli";
			break;
		case 8:
			echo "Avgust";
			break;
		case 9:
			echo "Septemvri";
			break;
		case 10:
			echo "Oktomvri";
			break;
		case 11:
			echo "Noemvri";
			break;
		case 12:
			echo "Dekemvri";
			break;
		default:
			echo "Za vneseniot broj ne postoi takov mesec";
			break;
	}
?>