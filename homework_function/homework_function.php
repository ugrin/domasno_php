<?php
	declare(strict_types=1);
	/**
	* Prva zadaca: da se pecati ime, prezime i datum na ragjanje
	*/
	function printNameSurnameDate(string $name, string $surname, string $date)
	{
		echo $name . ' ' . $surname . ' '. $date;
	}

	printNameSurnameDate('Angela','Ugrinovska','14.12.1991');

	echo '<br>';
	/**
	*Vtora zadaca: perimetar i plostina na pravoagolnik
	*/

	function calculatingPerimeterAreaRectangle(float $a, float $b)
	{
		$l = 2*($a + $b);
		$p = $a * $b;

		echo 'Perimetarot na pravoagolnikot e ' . $l . ' plostinata na pravoagolnikot e ' . $p;
	}

	calculatingPerimeterAreaRectangle(5,10);

	echo '<br>';
	/**
	*Treta zadaca: maksimum od tri broja
	*/
	function maxOfThreeNumbers(int $a, int $b, int $c):int
	{
		$max = $a;
		if($max < $b)
		{
			$max = $b;
		}
		if ($max < $c)
		{
			$max = $c;
		}

		return $max;
	}

	echo maxOfThreeNumbers(2,15,16);

	echo '<br>';

	/**
	*Cetvrta zadaca: Procent od eden broj vo drug broj
	*/

	function percentage(float $a, float $b)
	{
		$per = ($a / $b) * 100;
		$per = round($per);
		return "$per%"; 
	}

	echo "Rezultati od test: " . percentage(14.5,16) . '<br>';

	echo percentage(25,100);
?>

