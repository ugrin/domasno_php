<?php
	//prva zadaca - parni pozicii
	$niza = [3, 4, 12, 15, 24, 28, 33];

	for($i=0; $i < count($niza); $i++)
	{
		if($i % 2 != 0)
		{
			echo $niza[$i] . '<br>';
		}
	}

	$i=0;
	while ($i < count($niza)) 
	{
		if ($i % 2 != 0)
		{
			echo $niza[$i] . '<br>';
		}
	 $i++;
	}

	//vtora zadaca - neparni pozicii
	for($i=0; $i < count($niza); $i++)
	{
		if($i % 2 == 0)
		{
			echo $niza[$i] . '<br>';
		}
	}

	$i=0;
	while ($i < count($niza)) 
	{
		if($i % 2 == 0)
		{
			echo $niza[$i] . '<br>';
		}
	$i++;
	}

	//treta zadaca - 5ti element od niza
	for($i=0; $i < count($niza); $i++)
	{
		if($i == 4)
		{
			echo $niza[$i] . '<br>';
		}
	}

	$i=0;
	while ($i < count($niza)) 
	{
		if ($i == 4) {
			echo $niza[$i] . '<br>';
		}
	$i++;	
	}

	//cetvrta zadaca
	for($i=234; $i<=1987; $i++)
	{
		if($i % 3 == 0)
		{
			echo "$i; ";
		}
	}
	echo '<br>';
	$i=234;
	while($i<=1987)
	{
		if($i % 3 == 0)
		{
			echo "$i; ";
		}
	$i++;
	}

	echo '<br>';
	//petta zadaca - tablica mnozenje so 6
	$proizvod = 1;
	for($i=1; $i <= 10;$i++)
	{
		$proizvod = $i*6;
		echo "$i x 6 = $proizvod <br>";
	}
	$i=1;
	while($i <= 10)
	{
		$proizvod=$i*6;
		echo "$i x 6 = $proizvod <br>";
		$i++;
	}
?>