<?php

require_once 'database.php';
require_once 'vozilo.php';

$id = $_GET['id'];

$db = new VoziloDatabase();

$db->delete($id);

header('Location: dashboard.php');

?>