<?php

require_once 'database.php';

class VoziloDatabase extends Database
{
	public function __construct()
	{
		parent:: __construct();
	}

	public function print()
	{
		$statement = $this->connection->prepare('SELECT * FROM Vozilo');

		$statement->execute();

		$result = $statement->fetchAll(PDO::FETCH_ASSOC);

		return $result;
		
		// foreach($result as $vozilo)
		// {
		// 	echo 'Model: ' . $vozilo['model'] . '<br>' . 'Marka: ' . $vozilo['marka'] . '<br>' . 
		// 	'Godina na proizvodstvo: ' . $vozilo['godina'] . '<br>' . 'Cena: ' . $vozilo['cena'] . '<br>';
		// }
	}

	public function save($model, $marka, $godina, $cena)
	{
		$statement = $this->connection->prepare('INSERT INTO Vozilo(model,marka,godina,cena)
		 VALUES (:model,:marka,:godina,:cena)');

		$statement->bindParam(':model', $model);
		$statement->bindParam(':marka', $marka);
		$statement->bindParam(':godina', $godina);
		$statement->bindParam(':cena', $cena);	

		return $statement->execute();
	}

	public function update($id, $model, $marka, $godina, $cena)
	{
		$statement = $this->connection->prepare('UPDATE Vozilo SET model = :model, 
		marka = :marka, godina = :godina, cena = :cena WHERE id = :id');

		$statement->bindParam(':id', $id);
		$statement->bindParam(':model', $model);
		$statement->bindParam(':marka', $marka);
		$statement->bindParam(':godina', $godina);
		$statement->bindParam(':cena', $cena);

		$statement->execute();
	}

	public function getById($id)
	{
		$statement = $this->connection->prepare('SELECT * FROM Vozilo WHERE id = :id');

		$statement->bindParam(':id', $id);

		$statement->execute();


		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result;
	}

	public function delete($id)
	{
		$statement = $this->connection->prepare('DELETE FROM Vozilo WHERE id = :id');

		$statement->bindParam(':id', $id);

		$statement->execute();
	}
}

