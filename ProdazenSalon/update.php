<?php

require_once 'database.php';
require_once 'vozilo.php';

$id = $_GET['id'];

$model = $_POST['model'];
$marka = $_POST['marka'];
$godina = $_POST['godina'];
$cena = $_POST['cena'];

$db = new VoziloDatabase();

$db->update($id, $model, $marka, $godina, $cena);

header('Location: dashboard.php');

?>